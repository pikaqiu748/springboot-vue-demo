package com.li.dao;

import com.li.pojo.Books;
import org.springframework.data.jpa.repository.JpaRepository;


//JpaRepository已经实现了很多增删查改方法，所以只需要继承即可直接使用
public interface BookRepository extends JpaRepository<Books,Integer> {

}
