package com.li.controller;


import com.li.dao.BookRepository;
import com.li.pojo.Books;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.websocket.server.PathParam;
import javax.xml.soap.SAAJResult;
import java.awt.print.Book;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/book")
public class BookService {

    @Autowired
    private BookRepository bookRepository;

    @RequestMapping("/findAll/{page}/{size}")
    public Page<Books> findAll(@PathVariable("page") Integer page, @PathVariable("size") Integer size){
//        findAll()重载了很多方法
        Pageable pageable= PageRequest.of(page-1,size);
        return bookRepository.findAll(pageable);
    }


    @RequestMapping("/findById/{id}")
    public Books findById(@PathVariable("id") Integer id){
        Optional<Books> optionalBooks=bookRepository.findById(id);
        return optionalBooks.orElse(null);
    }

    @RequestMapping("/addBook/{name}/{count}/{detail}")
    public void addBook(@PathVariable("name") String name,@PathVariable("count") Integer count,@PathVariable("detail") String detail){
        Books books=new Books();

        books.setName(name);
        books.setCounts(count);
        books.setDetail(detail);

        bookRepository.save(books);
        books=null;
    }

    @RequestMapping("/save")
    public String save(@RequestBody Books books){
        Books res=bookRepository.save(books);
        if(res!=null){
            return "成功";
        }else {
            return "失败";
        }
    }

    @RequestMapping("/update")
    public void update(@RequestBody Books book){
//        save即可以保存，也可以修改，及=即如果不存在id,则保存，存在则ID不变，其它内容修改
       bookRepository.save(book);
    }

    @RequestMapping("/delete/{id}")
    public void delete(@PathVariable("id") Integer id){
         bookRepository.deleteById(id);
    }

}
