package com.li.pojo;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.Generated;
import org.springframework.data.repository.query.Param;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
//类名首字母小写，要和表名一样
public class Books {

    @Id
    @Generated    //表示是自增的,添加数据时，不用再手动添加id
    private int id;

    private String name;

    private int counts;

    private String detail;
}
