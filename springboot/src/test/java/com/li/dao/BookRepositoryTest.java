package com.li.dao;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;


@SpringBootTest
class BookRepositoryTest {

    @Autowired
    private BookRepository bookRepository;

    @Test
    public void findAll(){
        System.out.println(bookRepository.findAll());
    }

    @Test
    public void findById(){
        System.out.println( bookRepository.findById(1));
    }
}